import $ from 'jquery';
export default () => {
    (function (custom, $) {

        const custome = () => {
            $("input[type='radio']").change(function () {
                // $(this).parent().addClass("active-variant");
                var img = $(this).next().data("img");
                var id = $(this).next().data("pro-id");
                if (img) {
                    var newid = '#product-img-' + id;
                    $(newid).attr('src', img);
                }
            });
            $(".cart-block__search").click(function (e) {
                search_windo();
                console.log("done");
            });
            $(".clear_div").click(function (e) {
                search_windo();
            });

            function search_windo() {
                if ($(".search-box").hasClass('is-open')) {
                    $(".search-box").removeClass('is-open');
                    $(".search-box").addClass('is-close');
                } else {
                    $(".search-box").removeClass('is-close');
                    $(".search-box").addClass('is-open');
                }
            }
            $('.Modern-Slider').slick({
                dots: false,
                arrows: true,
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1
            });
            $('.color_Slider').slick({
                dots: false,
                arrows: true,
                infinite: true,
                slidesToShow: 10,
                slidesToScroll: 1
            });

            // $('.main-image').zoom({
            //     url: $(this).find('img').attr('data-zoom')
            // });

            $('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: true,
                fade: true,
                asNavFor: '.slider-nav',
                responsive: [
                    {
                      breakpoint: 600,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: true
                      }
                    },
                    {
                      breakpoint: 480,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true
                      }
                    }
                    ]
            });
            $('.slider-nav').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                dots: false,
                vertical:true,
                arrows: false,
                focusOnSelect: true
            });

            $("body").click(function () {
                    if ($(".search-box").hasClass('is-open')) {
                        // console.log("Body");
                        $(".search-box").removeClass('is-open');
                        $(".search-box").addClass('is-close');
                    };
                });

            $(".search-box").hasClass('is-open');

            $(document).ready(function() {
                $(".set > a").on("click", function() {
                  if ($(this).hasClass("active")) {
                    $(this).removeClass("active");
                    $(this)
                      .siblings(".content")
                      .slideUp(200);
                    $(this)
                      .find("i")
                      .removeClass("fa-minus")
                      .addClass("fa-plus");
                  } else {
                    // $(".set > a i")
                    //   .removeClass("fa-minus")
                    //   .addClass("fa-plus");
                    
                    // $(".set > a").removeClass("active");
                    $(this).addClass("active");
                    // $(".content").slideUp(200);
                    $(this)
                      .siblings(".content")
                      .slideDown(200);
                      $(this)
                      .find("i")
                      .removeClass("fa-plus")
                      .addClass("fa-minus");
                  }
                });
              });

              $(document).ready(function() {
                  $(".cate_color_li").click(function(){
                        $(".cate_color_li").removeClass("active_color");
                        $(this).addClass("active_color");                
                  });

                  $(".div_color_box").click(function(){
                        $(".div_color_box").removeClass("selected");
                        $(this).addClass("selected");                
                  });
              });

              //PDP Qty Selector
            $("button.qty-btns").on("click", function (event) {
                event.preventDefault();
                var container = $(event.currentTarget).closest('[data-qtyContainer]');
                var qtEle = $(container).find('[data-qty]');
                var currentQty = $(qtEle).val();
                var qtyDirection = $(this).data("direction");
                var newQty = 0;

                if (qtyDirection == "1") {
                    newQty = parseInt(currentQty) + 1;
                } else if (qtyDirection == "-1") {
                    newQty = parseInt(currentQty) - 1;
                }

                if (newQty == 1) {
                    $(".decrement-quantity").attr("disabled", "disabled");
                }
                if (newQty > 1) {
                    $(".decrement-quantity").removeAttr("disabled");
                }

                if (newQty > 0) {
                    newQty = newQty.toString();
                    $(qtEle).val(newQty);
                } else {
                    $(qtEle).val("1");
                }
            });
            
            $(document).ready(function(){
	
                $('ul.tabs li').click(function(){
                    var tab_id = $(this).attr('data-tab');
            
                    $('ul.tabs li').removeClass('current');
                    $('.tab-content').removeClass('current');
            
                    $(this).addClass('current');
                    $("#"+tab_id).addClass('current');
                });
            
            });
        }
        custom.init = function () {
            custome();
        };

    }(window.custom = window.Tabs || {}, $, undefined));
}