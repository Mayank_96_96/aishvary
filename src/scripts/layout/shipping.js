import $ from 'jquery';

export default () => {
    ((Shipping, $) => {
        'use strict';

        Shipping.update = () => {
            let total = parseInt(CartJS.cart.total_price),
                amount = parseInt(document.querySelector('[data-needed-amount]').dataset.neededAmount),
                needed = amount - total;
            if (needed > 0) {
                document.querySelector('[data-needed-text]').style.display = "block";
                document.querySelector('[data-unlock-text]').style.display = "none";
                document.querySelector('[data-needed-text]').querySelector('span').innerText = parseInt(needed / 100);
                let percent = (total / amount) * 100;
                document.querySelector('.shipping__needed').style.width = `${percent}%`
            }
            else {
                document.querySelector('[data-needed-text]').style.display = "none";
                document.querySelector('[data-unlock-text]').style.display = "block";
                document.querySelector('.shipping__needed').style.width = '100%'
            }
        };

    })(window.Shipping = window.Shipping || {}, $);
}
