import $ from 'jquery';

export default() => {
	((Navigation, $) => {
	  'use strict';

	  const $dom = {};

	  const cacheDom = () => {
	    $dom.mobileTrigger = $('[data-mobile-trigger]');
	    $dom.mobileDrawer = $('[data-mobile-drawer]');
	    $dom.mobileDrawerClose = $('[data-mobile-close]');
	    
	    $dom.bodyElement = $('body');
	    
	  };

	  const openMobileNav = () => {
	    $dom.mobileTrigger.addClass('is-open');
	    $dom.mobileDrawer.addClass('is-open');
	    $dom.bodyElement.addClass('no-scroll');
	  };

	  const closeMobileNav = () => {
	    $dom.mobileTrigger.removeClass('is-open');
	    $dom.mobileDrawer.removeClass('is-open');
	    $dom.bodyElement.removeClass('no-scroll');
	  };

	  const toggleMobileNav = () => {
	    if ($dom.mobileTrigger.hasClass('is-open')) {
	      closeMobileNav();
	    }
	    else {
	      openMobileNav();
	    }
	  };

	  const bindUIActions = () => {

	    $dom.mobileTrigger.on('click', function (event) {
	      event.preventDefault();
	      toggleMobileNav();
	    });

	    $dom.mobileDrawerClose.on('click', function (event) {
	      event.preventDefault();
	      closeMobileNav();
	    });

	    $(window).on('resize', function() {
	      if ($(window).width() >= 1023) {
	        closeMobileNav();
	      }
	    });

	  };

	  Navigation.init = () => {
	    cacheDom();
	    bindUIActions();
	  };

	})(window.Navigation = window.Navigation || {}, $);
}
