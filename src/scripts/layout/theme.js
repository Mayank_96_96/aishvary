import 'lazysizes/plugins/object-fit/ls.object-fit';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
import 'lazysizes/plugins/rias/ls.rias';
import 'lazysizes/plugins/bgset/ls.bgset';
import 'lazysizes';
import 'lazysizes/plugins/respimg/ls.respimg';

import $ from 'jquery';
window.jQuery = $;
window.$ = $;

import '../../styles/theme.scss';
import '../../styles/theme.scss.liquid';

import "../../styles/slick.scss";
import "../../styles/slick-theme.scss";

import shipping from './shipping';
import qty from './qty';
import loadMore from './LoadMore';
import navigation from './navigation';
import logo_slider from './logo_slider';
import custom from './custome';

import {
  focusHash,
  bindInPageLinks
} from '@shopify/theme-a11y';
import {
  cookiesEnabled
} from '@shopify/theme-cart';

logo_slider();
shipping();
qty();
navigation();
loadMore();
custom();

window.Qty.init();
window.custom.init();
window.Loadmore.init();
// Common a11y fixes
focusHash();
bindInPageLinks();


// Apply a specific class to the html element for browser support of cookies.
if (cookiesEnabled()) {
  document.documentElement.className = document.documentElement.className.replace(
    'supports-no-cookies',
    'supports-cookies',
  );
}

$(window).on('resize', function () {
  if ($(window).width() > 1024) {
    $('.drop').removeAttr('style');
    $('.mega-menu').removeAttr('style');
    $('body').removeClass('nav-active');
  }
});

$(window).on('load', function () {
  burgerMenu();
});

//Burger menu
function burgerMenu() {
  var opener = $('.nav-opener');
  opener.on('click', function (e) {
    $('body').toggleClass('nav-active');
    $(this).siblings('.drop').slideToggle();
    e.preventDefault();
  });

  $('.has-dropdown > a').click(function (e) {
    if ($(window).width() < 1024) {
      $(this).siblings('.mega-menu').slideToggle();
      e.preventDefault();
    }
  });
}

$(document).ready(function () {
  $('#tab_header ul li.item').on('click', function () {
    var number = $(this).data('option');
    $('#tab_header ul li.item').removeClass('is-active');
    $(this).addClass('is-active');
    $('#tab_container .container_item').removeClass('is-active');
    $('div[data-item="' + number + '"]').addClass('is-active');
  });
});

$(document).ready(function () {
  $('#tab_header ul li.tab-list__item').on('click', function () {
    var number = $(this).data('option');
    $('#tab_header ul li.tab-list__item').removeClass('is-active');
    $(this).addClass('is-active');
    $('#tab_container .container_item').removeClass('is-active');
    $('div[data-item="' + number + '"]').addClass('is-active');
  });
});

$(document).ready(function () {
  
  $(".index-banner-slider").slick({
    dots: false,
    arrows: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1
  });

  $('.logo-slider').slick({
    dots: false,
    arrows: true,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 7,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
          infinite: true
        }
      },
      {
        breakpoint: 545,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true
        }
      }, {
        breakpoint: 375,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true
        }
      }
    ]
  });

  $('.menu_slider_div').slick({
    dots: false,
    arrows: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1
});
})