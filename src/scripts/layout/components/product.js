const Product = (product) => {
    const buy = `<button type="submit" data-cart-add="${product.variants[0].id}" class="c-btn c-btn--medium c-productList__btn">+ Add</button>`;
    const soldout = `<button type="button" class="c-btn disabled c-btn--medium c-productList__btn">SOLD OUT</button>`;

    return `
    <div class="o-flex__item c-newProducts__item" 
    data-product-type="${product.type}" 
    data-product-style="${product.style}"
    data-product-title="${product.title}"
    data-product-color="${product.color}">
        <div class="c-productList u-textCenter">
            <div class="c-productList__imageCol">
                <a href="${product.url}">
                    <img src="${product.image}" alt="${product.title}" class="c-productList__image" />
                </a>  
            </div>
            <div class="c-productList__contentCol">
                <div class="c-productList__item">
                    <h4 class="c-heading3 c-productList__heading">
                        <a href="${product.url}">
                            ${product.title}
                        </a>
                    </h4>
                    <div class="c-productList__price money">
                        ${product.price}
                        ${product.comparePrice ? `<strike>${product.comparePrice}</strike>` : ''}
                    </div>
                </div>
            </div> 
        
            <div class="c-productList__review u-textCenter">
                <div data-oke-reviews-product-listing-rating></div>    
            </div>
            <form action="/cart/add" 
                method="post" 
                enctype="multipart/form-data" 
                data-cart-submit="data-cart-submit" 
                data-product-id="${product.variants[0].id}" 
                class="c-productList__form">
                ${product.variants[0].available ? buy : soldout}
            </form>
        </div>
    </div>
    `
};

export default Product