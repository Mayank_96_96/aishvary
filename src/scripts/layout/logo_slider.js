import $ from 'jquery';
import slick from 'slick-carousel';

export default () => {
    (function (logo_slider, $) {

        const logo_sliderInit = () => {

            $('.logo-slider').slick({
                dots: false,
                arrows: false,
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }]
            });
        };

        const menu_sliderInit = () => {

            $('.menu_slider_div').slick({
                dots: false,
                arrows: true,
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        };
        const color_swatchInit = () => {
            $('.swatch').slick({
                dots: false,
                arrows: true,
                infinite: false,
                slidesToShow: 5,
                slidesToScroll: 1
            });
        };
        

        logo_slider.init = function () {
            logo_sliderInit();
            menu_sliderInit();
            color_swatchInit();
            
        };

    }(window.logo_slider = window.logo_slider || {}, $, undefined));
}