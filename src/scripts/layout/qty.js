import $ from 'jquery';

export default() => {
	(function (Qty, $) {
	
	  	const qtyInit = () => {

			$('.js-plus').click(function () {
				var target = $(this);
				var container = $(target).closest('[data-cartitem]');
				var randomNumber = $(container).data('random');				
				if ($(this).prev().val() < 10) {
					var qtyValue  = parseInt($(this).prev().val()) + 1;
					$(this).prev().val(qtyValue);
					if( randomNumber != null && randomNumber != undefined && randomNumber != ''){
						$('[data-random="'+randomNumber+'"]').find('[name="updates[]"]').val(qtyValue);
					}
				}
				
			});
			
			$('.js-minus').click(function () {
				var target = $(this);
				var container = $(target).closest('[data-cartitem]');
				var randomNumber = $(container).data('random');
				if ($(this).next().val() > 1) {
					var qtyValue  = parseInt($(this).next().val()) - 1;
					$(this).next().val(qtyValue);
					if( randomNumber != null && randomNumber != undefined && randomNumber != ''){
						$('[data-random="'+randomNumber+'"]').find('[name="updates[]"]').val(qtyValue);
					}
				}
			});
		};

	  Qty.init = function () {
	    qtyInit();
	  };

	}(window.Qty = window.Tabs || {}, $, undefined));
}


