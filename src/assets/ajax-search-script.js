$(function () {
    var currentAjaxRequest = null;
    var searchForms = $('form[action="/search"]').css('position', 'relative').each(function () {
        var input = $(this).find('input[name="q"]');
        input.attr('autocomplete', 'off').bind('keyup change', function () {
            var term = $(this).val();
            var form = $(this).closest('form');
            var searchURL = '/search?type=product&q=*' + term + '*';
            var resultsList = $('.search-results');
            $(".clear_div").click(function (e) {
                e.preventDefault();
                $(resultsList).empty();
                $(input).val('');
                $(".search-box").removeClass('is-open');
                $(".search-box").addClass('is-close');
            });
            if (term.length > 3 && term !== $(this).attr('data-old-term')) {
                $(this).attr('data-old-term', term);
                if (currentAjaxRequest !== null) currentAjaxRequest.abort();
                currentAjaxRequest = $.getJSON(searchURL + '&view=json', function (data) {
                    resultsList.empty();
                    if (data.results_count === 0) {
                        resultsList.html('<p>No results.</p>');
                        resultsList.fadeIn(200);
                        resultsList.hide();
                    } else {
                        $.each(data.results, function (index, item) {
                            if (index <= 3) {
                                var max_img = item.thumbnail;
                                var avoid = "_thumb";
                                var orig_img = max_img.replace(avoid, '');
                                var link = $('<a></a>').attr('href', item.url);
                                link.append('<div class="img-list"><img src="' + orig_img + '"/></div>');
                                link.append('<div class="product_title">' + item.title + '</div>');
                                link.wrap('<div class="column is-half-mobile is-3"></div>');
                                resultsList.append(link.parent());
                            }
                        });
                        if (data.results_count > 4) {
                            resultsList.append('<div class="row"><div class="semi-10 large-push-2 semi-push-1 large-8 columns"><a class="btn" href="' + searchURL + '"> +(' + data.results_count + ') more</a></div></div>');
                        }
                        resultsList.fadeIn(200);
                    }
                });
            }
        });
    });
});